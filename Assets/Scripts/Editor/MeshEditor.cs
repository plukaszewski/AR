using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.VisualScripting;

public static class MeshEditor

{
    [MenuItem("CONTEXT/MeshFilter/DebugMesh")]
    public static void DebugMesh(MenuCommand command)
    {
        Mesh mesh = command.context.GetComponent<MeshFilter>().mesh;

        foreach (var item in mesh.uv)
        {
            Debug.Log(item);
        }
    }

    [MenuItem("CONTEXT/MeshFilter/SaveMesh")]
    public static void SaveMesh(MenuCommand command)
    {
        MeshFilter meshFilter = command.context.GetComponent<MeshFilter>();
        Mesh mesh = meshFilter.sharedMesh;

        string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/", mesh.name, "asset");
        if (string.IsNullOrEmpty(path)) return;

        path = FileUtil.GetProjectRelativePath(path);

        Mesh meshToSave = CombineMeshes(new Mesh[] { mesh }, new Matrix4x4[] { meshFilter.transform.localToWorldMatrix });

        MeshUtility.Optimize(meshToSave);

        AssetDatabase.CreateAsset(meshToSave, path);
        AssetDatabase.SaveAssets();

    }

	[MenuItem("CONTEXT/MeshFilter/SaveCombinedMesh")]
	public static void SaveCombinedMesh(MenuCommand command)
	{
		MeshFilter meshFilter = command.context.GetComponent<MeshFilter>();
		Mesh mesh = meshFilter.sharedMesh;

		string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/", "CombinedMesh", "asset");
		if (string.IsNullOrEmpty(path)) return;

		path = FileUtil.GetProjectRelativePath(path);

        List<Mesh> meshes = new List<Mesh>();
        List<Matrix4x4> matrixes = new List<Matrix4x4>();

        foreach (var item in meshFilter.GetComponentsInChildren<MeshFilter>())
        {
            if (!item.sharedMesh)
                continue;

            meshes.Add(item.sharedMesh);
			matrixes.Add(item.transform.localToWorldMatrix);
		}

		Mesh meshToSave = CombineMeshes(meshes.ToArray(), matrixes.ToArray());

		MeshUtility.Optimize(meshToSave);

		AssetDatabase.CreateAsset(meshToSave, path);
		AssetDatabase.SaveAssets();
	}

	public static Mesh CombineMeshes(Mesh[] meshes, Matrix4x4[] transforms)
    {
        CombineInstance[] c = new CombineInstance[meshes.Length];
        
        for (int i = 0; i < meshes.Length; i++)
        {
            c[i].mesh = meshes[i];
            c[i].transform = transforms[i];
        }

        Mesh mesh = new();
        mesh.CombineMeshes(c);

        return mesh;
    }
}
