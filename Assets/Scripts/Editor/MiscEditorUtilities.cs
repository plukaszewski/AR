using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Unity.VisualScripting;

public static class EditorUtilities
{
	const int PREVIEW_SIZE = 256;

	[MenuItem("CONTEXT/Part/Save mesh preview")]
	public static void SaveMeshPreview(MenuCommand command)
	{
		Element part = command.context.GetComponent<Element>();
		Mesh mesh = part.Mesh;

		string prefabPath = AssetDatabase.GetAssetPath(command.context);
		if (string.IsNullOrEmpty(prefabPath))
			prefabPath = "Assets/";

		string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", prefabPath, mesh.name, "png");
		if (string.IsNullOrEmpty(path)) return;

		Texture2D texture;
		using (MeshPreview meshPreview = new MeshPreview(mesh))
		{
			texture = meshPreview.RenderStaticPreview(PREVIEW_SIZE, PREVIEW_SIZE);
		}


		System.IO.File.WriteAllBytes(path, texture.EncodeToPNG());
		AssetDatabase.Refresh();

		string relativepath = "";
		if (path.StartsWith(Application.dataPath))
		{
			relativepath = "Assets" + path.Substring(Application.dataPath.Length);
		}

		TextureImporter textureImporter = AssetImporter.GetAtPath(relativepath) as TextureImporter;
		textureImporter.textureType = TextureImporterType.Sprite;
		AssetDatabase.ImportAsset(relativepath);

		typeof(Element).GetField("_icon", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Default).SetValue(part, AssetDatabase.LoadAssetAtPath<Sprite>(relativepath));
	}
}
