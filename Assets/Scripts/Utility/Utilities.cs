using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Utilities
{
    public static bool ContainsAny<T>(this IEnumerable<T> array, IEnumerable<T> other)
    {
        foreach (var item in other)
        {
            if(array.Contains(item))
                return true;
        }

        return false;
    }
}
