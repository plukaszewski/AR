using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class Global
{
    private static Controls _input;
    public static Controls Input
    {
        get
        {
            if (_input == null)
            {
                _input = new Controls();
                _input.Enable();
                _input.Camera.Enable();
                _input.Player.Enable();
                _input.Inventory.Enable();

                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            return _input;
        }
    }

    public static CameraRaycast CameraRaycast;
    public static TicTacToe Game;
    public static Grid3DWorldSpace<Element> Grid;
    public static TicTacToe.Settings GameSettings;

    public static UnityEvent OnSubmit { get; } = new();
    public static UnityEvent OnCancel { get; } = new();
    public static UnityEvent OnNeedConfirmation { get; } = new();
    public static UnityEvent OnGameStarted { get; } = new();
    public static UnityEvent<Element.Type> OnGameOver { get; } = new();
}
