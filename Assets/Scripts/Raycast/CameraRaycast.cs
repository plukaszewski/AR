using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour, IRaycast
{
	private GameObject _lookingAtObject;
	public GameObject LookingAtObject => _lookingAtObject;
	private RaycastHit _lookingAtHit;
	public RaycastHit LookingAtHit => _lookingAtHit;

	private void Awake()
	{
		Global.CameraRaycast = this;
	}

	// Update is called once per frame
	void Update()
	{
		if (Physics.Raycast(Camera.main.ScreenPointToRay(Global.Input.Player.MousePosition.ReadValue<Vector2>()), out RaycastHit hit))
		{
			_lookingAtHit = hit;
			_lookingAtObject = hit.transform.gameObject;
		}
		else
		{
			_lookingAtObject = null;
			_lookingAtHit = default;
		}
	}

	public GameObject GetLookingAtObject(Ray ray)
	{
		Physics.Raycast(ray, out RaycastHit hit);
		return hit.collider.gameObject;
	}

	public RaycastHit GetLookingAtHit(Ray ray)
	{
		Physics.Raycast(ray, out RaycastHit hit);
		return hit;
	}

	public void DebugRaycast()
	{
		Ray r = Camera.main.ScreenPointToRay(Global.Input.Player.MousePosition.ReadValue<Vector2>());
		Debug.DrawRay(r.origin, r.direction * 10f, Color.red, 2f);
		Debug.Log($"{r.origin}, {r.direction}");
	}
}
