using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRaycast
{
	public GameObject LookingAtObject { get; }
	public GameObject GetLookingAtObject(Ray ray);
}
