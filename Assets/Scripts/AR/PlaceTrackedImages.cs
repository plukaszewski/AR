using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARTrackedImageManager))]
public sealed class PlaceTrackedImages : MonoBehaviour
{
	[SerializeField]
	private TrackedImageToPrefabMap _imageMap;

	[SerializeField]
    private ARTrackedImageManager _trackedImageManager;

	private Dictionary<string, GameObject> _instantiatedPrefabs = new();

	[SerializeField]
	private TMPro.TextMeshProUGUI _textMeshPro;

    private void OnValidate()
    {
        _trackedImageManager = GetComponent<ARTrackedImageManager>();
    }

    private void Awake()
	{
		Debug.LogWarning("Awake");
	}

    private void Start()
    {
        Debug.LogWarning("Start");
        foreach (var item in _imageMap.Dict)
        {
            Debug.Log(item.Value);
        }
    }

    private void OnEnable()
	{
		_trackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
		Application.logMessageReceived += Log;
	}

	private void OnDisable()
	{
		_trackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
		Application.logMessageReceived -= Log;
    }

    private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs args)
	{
        foreach (var item in args.added)
		{
			AddTrackedImage(item);
        }

        foreach (var item in args.updated)
        {
            UpdateTrackedImage(item);
        }

        foreach (var item in args.removed)
		{
			RemoveTrackedImage(item);
        }
	}

	private void AddTrackedImage(ARTrackedImage image)
	{
		string name = image.referenceImage.name;
		Debug.Log($"_{_imageMap.Dict[image.referenceImage]}_, {image.transform}");
        var prefab = Instantiate(_imageMap.Dict[image.referenceImage], image.transform);
        _instantiatedPrefabs.Add(name, prefab);
	}

	private void RemoveTrackedImage(ARTrackedImage image)
	{
		Destroy(_instantiatedPrefabs[image.referenceImage.name]);
		_instantiatedPrefabs.Remove(image.referenceImage.name);
	}

	private void UpdateTrackedImage(ARTrackedImage image)
	{
		_instantiatedPrefabs[image.referenceImage.name].SetActive(image.trackingState == TrackingState.Tracking);
	}

	private void Log(string log, string stack, LogType type)
	{
		_textMeshPro.text += $"\n{log}, {stack}";
	}
}
