using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[CreateAssetMenu(fileName = "new map")]
public sealed class TrackedImageToPrefabMap : ScriptableObject
{
	[SerializeField]
	private XRReferenceImageLibrary _library;

	[SerializeField]
	private List<Mapping> _map;

	[SerializeField]
	private int a;

	private Dictionary<XRReferenceImage, GameObject> _dict;

	public Dictionary<XRReferenceImage, GameObject> Dict
	{
		get
		{
			if(_dict == null)
			{
				_dict = new();
                foreach (var item in _map)
                {
                    Dict.Add(item.image, item.prefab);
                    Debug.Log($"{item.image}, {item.prefab}");
                }
            }

			return _dict;
		}
	}


	[System.Serializable]
	private struct Mapping
	{
		public XRReferenceImage image;
		public GameObject prefab;
	}

	private void OnValidate()
	{
		Mapping[] tmp = new Mapping[_map.Count];
		_map.CopyTo(tmp);
        List<Mapping> tmpMap = new List<Mapping>(tmp);
		_map.Clear();
		
		foreach (var item in _library)
		{
			int n = tmpMap.FindIndex((x) => x.image == item);

			if (n == -1)
				_map.Add(new Mapping { image = item });
			else
				_map.Add(tmpMap[n]);
		}

		Dict.Clear();
		foreach (var item in _map)
		{
			Dict.Add(item.image, item.prefab);
			Debug.Log($"{item.image}, {item.prefab}");
		}
		Debug.Log("validate");
	}
}
