using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public sealed class Player : MonoBehaviour
{
	private void Awake()
	{
		Global.Input.Player.Use.performed += UseCallback;
	}

	private void OnDisable()
	{
		Global.Input.Player.Use.performed -= UseCallback;
	}

	private void UseCallback(InputAction.CallbackContext context)
	{
		//This is stupid, input performed fires before updating the cursor position
		StartCoroutine(CCallback());
	}

	private IEnumerator CCallback()
	{
		yield return new WaitForEndOfFrame();
		Global.CameraRaycast.DebugRaycast();
		if (Global.CameraRaycast.LookingAtObject == null)
			yield break;

		if (!Global.CameraRaycast.LookingAtObject.TryGetComponent(out Indicator outIndicator))
			yield break;

		outIndicator.Add();
	}
}
