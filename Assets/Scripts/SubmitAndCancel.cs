using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class SubmitAndCancel : MonoBehaviour
{
	public GameObject UI;
	public Button SubmitButton;
	public Button CancelButton;
	
	[ContextMenu("Submit")]
    public void Submit()
    {
        Global.OnSubmit.Invoke();
		HideUI();
	}

    [ContextMenu("Cancel")]
    public void Cancel()
	{
        Global.OnCancel.Invoke();
		HideUI();
	}

	public void ShowUI()
	{
		UI.SetActive(true);
	}

	public void HideUI()
	{
		UI.SetActive(false);
	}

	private void Start()
	{
		Global.OnNeedConfirmation.AddListener(ShowUI);
		SubmitButton.onClick.AddListener(Submit);
		CancelButton.onClick.AddListener(Cancel);
	}

	private void OnEnable()
	{
		Global.Input.Player.Submit.performed += OnSubmit;
		Global.Input.Player.Cancel.performed += OnCancel;
	}

	private void OnDisable()
	{
		Global.Input.Player.Submit.performed -= OnSubmit;
		Global.Input.Player.Cancel.performed -= OnCancel;
	}

	private void OnSubmit(InputAction.CallbackContext context)
	{
		Submit();
	}

	private void OnCancel(InputAction.CallbackContext context)
	{
		Cancel();
	}
}
