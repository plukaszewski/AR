using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Inventory : MonoBehaviour
{
	private int _index;
	private int Index
	{
		get => _index;
		set
		{
			_slots[Index].Selected = false;
			_index = value;
			_slots[Index].Selected = true;
			OnSelectionChanged.Invoke();
		}
	}

	public Element CurrentItem => _slots[Index].Part;

	public UnityEvent OnSelectionChanged { get; } = new();

	[SerializeField]
    private Slot _slotPrefab;

	[SerializeField]
	private Transform _content;

	[SerializeField]
	private InventoryCollection _inventoryCollection;

	private List<Slot> _slots = new();

	private void Awake()
	{
		Global.Input.Inventory.Next.performed += NextInput;
		Global.Input.Inventory.Previous.performed += PreviousInput;

		int i = 0;
		foreach (var item in _inventoryCollection)
		{
			int a = i++;
			Slot s = Instantiate(_slotPrefab);
			s.Part = item;
			//s.Sprite = item.Icon;
			s.transform.parent = _content;
			s.Button.onClick.AddListener(() => Select(a));
			_slots.Add(s);
		}

		Index = 0;
	}

	private void OnDestroy()
	{
		//Global.InventoryCollection = null;
		Global.Input.Inventory.Next.performed -= NextInput;
		Global.Input.Inventory.Previous.performed -= PreviousInput;
	}

	private void NextInput(InputAction.CallbackContext callbackContext)
	{
		Next();
	}

	private void Next()
	{
		Index = Mathf.Clamp(_index + 1, 0, _slots.Count - 1);
	}

	private void PreviousInput(InputAction.CallbackContext callbackContext)
	{
		Previous();
	}

	private void Previous()
	{
		Index = Mathf.Clamp(_index - 1, 0, _slots.Count - 1);
	}

	private void Select(int index)
	{
		Index = index;
	}
}
