using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item Collection")]
public class InventoryCollection : ScriptableObject, IEnumerable<Element>
{
	public Element[] items;

	public IEnumerator<Element> GetEnumerator()
	{
		foreach(var item in items)
			yield return item;
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return items.GetEnumerator();
	}
}
