using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
	private bool _selected;
	public bool Selected
	{
		get => _selected;
		set
		{
			_selected = value;
			_outline.SetActive(_selected);
		}
	}

	public Sprite Sprite
	{
		get => _image.sprite;
		set => _image.sprite = value;
	}

	[SerializeField]
	private Button _button;
	public Button Button => _button;

	public Element Part { get; set; }

	[SerializeField]
	private GameObject _outline;
	[SerializeField]
	private Image _image;
	
}
