using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresetSetter : MonoBehaviour
{
    public Button button;
    public Preset preset;
    public Menu menu;

    private void Start()
    {
        button.onClick.AddListener(() => menu.SetPreset(preset.settings));
    }
}
