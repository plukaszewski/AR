using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraMovement : MonoBehaviour
{
	public float MovementSpeed;
	public float RotationSpeed;

    private void Update()
    {
        transform.Translate(Global.Input.Camera.Movement.ReadValue<Vector3>() * Time.deltaTime * MovementSpeed);

        Vector2 input = Global.Input.Camera.Rotation.ReadValue<Vector2>();

        transform.Rotate(Vector3.up, input.x * Time.deltaTime * RotationSpeed, Space.World);
        transform.Rotate(transform.right, -input.y * Time.deltaTime * RotationSpeed, Space.World);
    }
}
