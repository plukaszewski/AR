using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Preset", menuName = "Preset")]
public class Preset : ScriptableObject
{
    public TicTacToe.Settings settings;
}
