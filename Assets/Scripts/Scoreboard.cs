using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
    public Transform content;
    public TMPro.TextMeshProUGUI labelPrefab;

    private Dictionary<Element.Type, TMPro.TextMeshProUGUI> _dict = new();

    private void Start()
    {
        Global.OnGameStarted.AddListener(Init);
    }

    private void Init()
    {
        Global.OnGameStarted.RemoveListener(Init);
        Global.Game.OnPlacedElement.AddListener(UpdateScore);

        foreach (var item in Global.Game.Players)
        {
            _dict.Add(item, Instantiate(labelPrefab, content));
        }

        UpdateScore();
    }

    private void UpdateScore()
    {
        foreach (var item in Global.Game.Players)
        {
            _dict[item].text = $"{item}: {Global.Game.PlayerScore[item]}";
        }
    }
}
