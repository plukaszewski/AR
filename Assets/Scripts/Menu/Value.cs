using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Value : MonoBehaviour
{
    [SerializeField]
    private TMPro.TextMeshProUGUI _valueLabel;

	[SerializeField]
	private Button _lowerButton;

	[SerializeField]
	private Button _higherButton;

	public Vector2Int range;

	private int _value;
    public int ValueInt
    {
        get => _value;
        set
        {
			_value = Mathf.Clamp(value, range.x, range.y);
			_valueLabel.text = _value.ToString();
		}
    }

	private void Start()
	{
		_value = int.Parse(_valueLabel.text);
		_lowerButton.onClick.AddListener(() => ValueInt--);
		_higherButton.onClick.AddListener(() => ValueInt++);
	}
}
