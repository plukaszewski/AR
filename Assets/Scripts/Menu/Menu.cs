using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
	public string SceneName;
	public Button PlayButton;

	public Value SizeX;
	public Value SizeY;
	public Value SizeZ;
	public Value Players;
	public Value WinningLines;
	public Value MinLineLength;

	private void Start()
	{
		PlayButton.onClick.AddListener(Play);
	}

	public void Play()
	{
		Global.GameSettings = new()
		{
			players = PLAYERS[0..Players.ValueInt],
            size = new Vector3Int(SizeX.ValueInt, SizeY.ValueInt, SizeZ.ValueInt),
			LinesToWin = WinningLines.ValueInt,
			MinLineLength = MinLineLength.ValueInt,
		};
		SceneManager.LoadScene(SceneName);
	}

	public void SetPreset(TicTacToe.Settings settings)
	{
		SizeX.ValueInt = settings.size.x;
		SizeY.ValueInt = settings.size.y;
		SizeZ.ValueInt = settings.size.z;
		Players.ValueInt = settings.players.Length;
		WinningLines.ValueInt = settings.LinesToWin;
		MinLineLength.ValueInt = settings.MinLineLength;
	}

	private readonly Element.Type[] PLAYERS = new Element.Type[] { Element.Type.X, Element.Type.O, Element.Type.C };
}
