using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.ARFoundation;

public sealed class GameStarter : MonoBehaviour
{
	public ARPlaneManager aRPlaneManager;
	public TicTacToe GamePrefab;

	private TicTacToe _spawned;
	private IndicatorDisplayer _indicatorDisplayer;

	private void OnEnable()
	{
		Global.Input.Player.Use.performed += Use;
	}

	private void OnDisable()
	{
		Global.Input.Player.Use.performed -= Use;
	}

	private void Start()
	{
		Global.OnSubmit.AddListener(StartGame);
	}

	private void Use(InputAction.CallbackContext context)
	{
		StartCoroutine(UseC());
	}

	private IEnumerator UseC()
	{
		yield return null;
		if (!Global.CameraRaycast.LookingAtObject || !Global.CameraRaycast.LookingAtObject.TryGetComponent(out ARPlane ARPlane))
			yield break;

		if (!_spawned)
		{
			_spawned = Instantiate(GamePrefab, Global.CameraRaycast.LookingAtHit.point, Quaternion.identity);
			_indicatorDisplayer = _spawned.GetComponent<IndicatorDisplayer>();
			Init();
			Global.OnNeedConfirmation.Invoke();
			yield break;
		} 

		_spawned.transform.position = Global.CameraRaycast.LookingAtHit.point;
		Global.OnNeedConfirmation.Invoke();
	}

	[ContextMenu("Start")]
    public void Init()
    {
		_spawned.Init(Global.GameSettings);
		_indicatorDisplayer.Init();
	}

	public void StartGame()
	{
		foreach (var item in FindObjectsByType<ARPlane>(FindObjectsSortMode.None))
		{
			item.gameObject.SetActive(false);
		}
		
		Global.Input.Player.Use.performed -= Use;
		_spawned.StartGame();
		_indicatorDisplayer.StartGame();
		Global.OnSubmit.RemoveListener(StartGame);
		aRPlaneManager.gameObject.SetActive(false);
		enabled = false;
	}
}
