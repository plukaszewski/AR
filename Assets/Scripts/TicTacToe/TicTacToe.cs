using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.Events;

public sealed class TicTacToe : MonoBehaviour
{
	[SerializeField]
	private Transform _base;

	[SerializeField]
	private Transform _cylinderPrefab;

	[SerializeField]
	private Transform _cylinderParent;

	[SerializeField]
	private AudioSource _placeSound;

    [SerializeField]
    private AudioSource _lineSound;

    [SerializeField]
    private AudioSource _winSound;

    public Vector3Int Size = new Vector3Int(3, 3, 3);
    public int WinningLines = 3;
    public int MinLineLength = 3;

	public Vector3 Offset;
	public Vector3 CellSize;
	public Quaternion Rotation;

	public Grid3DWorldSpace<Element> Grid;

	public Element.Type CurrentPlayer => Players[_playersIndex];

	public Dictionary<Element.Type, int> PlayerScore = new();

	public Element.Type[] Players { get; private set; }

	private int __playersIndex;
	private int _playersIndex
	{
		get => __playersIndex;
		set
		{
			__playersIndex = ++__playersIndex % Players.Length;
		}
	}

	public UnityEvent OnPlacedElement { get; } = new();
	public UnityEvent OnNextTurn { get; } = new();

	public void Init(Settings settings)
	{
		Size = settings.size;
		WinningLines = settings.LinesToWin;
		MinLineLength = settings.MinLineLength;

		Grid = new(Size.x, Size.y, Size.z);
		Grid.SetOffsetCellSizeAndRotation(transform.position + Offset, CellSize, transform.rotation * Rotation);
		Players = settings.players;

		_base.localScale =  new Vector3(Size.x * .1f, .02f, Size.z * .1f);
		_base.localPosition = new Vector3(Size.x * .05f, -.01f, Size.z * .05f);

		for (int i = 0; i < Size.x; i++)
			for (int j = 0; j < Size.z; j++)
			{
				Transform tmp = Instantiate(
					_cylinderPrefab, 
					transform.position + 
					Offset + 
					Vector3.right * CellSize.x * i + 
					Vector3.up * CellSize.y * Size.y * .5f + 
					Vector3.up * -Offset.y + 
					Vector3.forward * CellSize.z * j,
					Quaternion.identity, 
					_cylinderParent);
				tmp.localScale = new Vector3(.01f, CellSize.y * Size.y * .5f, .01f);
			}

		foreach (var type in Players)
		{
			PlayerScore[type] = 0;
		}

		Global.Grid = Grid;
		Global.Game = this;
	}

	public void StartGame()
	{
		UpdatePositionAndRotation();
		Global.OnGameStarted.Invoke();
	}

	public int GetColumnHeight(int x, int z)
	{
		int n = 0;
		for (int i = 0; i < Grid.Height; i++)
		{
			if (Grid[x, i, z] != null)
				n++;
			else
				break;
		}

		return n;
	}

	public void Place(int x, int y, int z, Element item)
	{
        Grid[x, y, z] = item;
		int n = ChceckIfInLine(x, y, z);
		if (n > 0)
		{
			PlayerScore[item.ElementType] += n;
			_lineSound.Play();
        }
        
        _placeSound.Play();
        OnPlacedElement.Invoke();

		if (PlayerScore[item.ElementType] >= WinningLines)
		{
            _winSound.Play();
            Global.OnGameOver.Invoke(item.ElementType);
			return;
		}
			
		_playersIndex++;
		OnNextTurn.Invoke();
	}

	public void UpdatePositionAndRotation()
	{
		Grid.SetOffsetCellSizeAndRotation(transform.position + Offset, CellSize, transform.rotation * Rotation);
	}

	private int ChceckIfInLine(int x, int y, int z)
    {
		int score = 0;
		Vector3Int pos = new Vector3Int(x, y, z);

		foreach(var direction in DIRECTIONS)
		{
			int n1 = 1;
			int n2 = 1;
			Vector3Int nextCell = pos + direction * n1;

			while (IsInsideTheBounds(nextCell) && Grid[nextCell] && Grid[pos].ElementType == Grid[nextCell].ElementType)
			{
				n1++;
				nextCell = pos + direction * n1;
			}

			nextCell = pos + direction * -n2;

			while (IsInsideTheBounds(nextCell) && Grid[nextCell] && Grid[pos].ElementType == Grid[nextCell].ElementType)
			{
				n2++;
				nextCell = pos + direction * -n2;
			}

			if (n1 + n2 - 1 >= MinLineLength)
			{
				for (int i = 0; i < n1; i++)
				{
					Grid[pos + direction * i].Highlight();
				}

				for (int i = 0; i < n2; i++)
				{
					Grid[pos - direction * i].Highlight();
				}

				score++;
			}
			
		}

		return score;
    }

	private bool IsInsideTheBounds(Vector3Int pos) => IsInsideTheBounds(pos.x, pos.y, pos.z);

	private bool IsInsideTheBounds(int x, int y, int z) =>
			x < Size.x && x >= 0 &&
			y < Size.y && y >= 0 &&
			z < Size.z && z >= 0;

	private readonly Vector3Int[] DIRECTIONS = new Vector3Int[]
	{
		new ( 1, 0, -1),
		new ( 1, 0,  0),
		new ( 1, 0,  1),
		new ( 0, 0,  1),
		new ( 0, 1,  1),
		new ( 1, 1,  1),
		new ( 1, 1,  0),
		new ( 1, 1, -1),
		new ( 0, 1, -1),
		new (-1, 1, -1),
		new (-1, 1,  0),
		new (-1, 1,  1),
		new ( 0, 1,  0),
	};

	[System.Serializable]
	public struct Settings
	{
		public Element.Type[] players;
        public Vector3Int size;
		public int MinLineLength;
		public int LinesToWin;
	}
}
