using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    public Button PlayAgainButton;
	public Transform WinUI;
	public TMPro.TextMeshProUGUI Label;

	public string MenuSceneName;

	private void Awake()
	{
		PlayAgainButton.onClick.AddListener(GoToMenu);
		Global.OnGameOver.AddListener(Show);
	}

	public void Show(Element.Type winning)
	{
		WinUI.gameObject.SetActive(true);

		if ((int)winning == -1)
			Label.text = "DRAW";
		else
			Label.text = $"WINNER: {winning}";
	}

	private void GoToMenu()
	{
		SceneManager.LoadScene(MenuSceneName);
	}
}
