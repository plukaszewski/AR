using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class IndicatorDisplayer : MonoBehaviour
{
	public Indicator Indicator;
	public Transform DefaultParent;

	public Element[] Prefabs;

	private Dictionary<Element.Type, Element> _prefabs = new();
	private List<Indicator> _indicators = new();

	public void Init()
	{
		foreach (var pref in Prefabs)
		{
			_prefabs[pref.ElementType] = pref;
		}
		Global.Game.OnNextTurn.AddListener(Refresh);
		Global.OnGameOver.AddListener(GameOver);
		//Global.OnCancel.AddListener(Refresh);
		
	}

	public void StartGame()
	{
		Refresh();
	}

	public void Refresh()
	{
		foreach (var indicator in _indicators)
		{
			Destroy(indicator.gameObject);
		}
		_indicators.Clear();

		Element current = _prefabs[Global.Game.CurrentPlayer];
		Vector3 size = Global.Game.Size;

		for(int i = 0; i < size.x; i++)
			for (int j = 0; j < size.z; j++)
			{
				int columnHeight = Global.Game.GetColumnHeight(i, j);
				if (columnHeight >= size.y)
					continue;

				Indicator tmp = Instantiate(Indicator, Global.Game.Grid.ToWorldSpace(i, size.y, j), Quaternion.identity, DefaultParent);
				tmp.Set(i, columnHeight, j, current);
				_indicators.Add(tmp);
			}
	}

	public void GameOver(Element.Type type)
	{
		foreach (var indicator in _indicators)
		{
			Destroy(indicator.gameObject);
		}
		_indicators.Clear();
	}
}