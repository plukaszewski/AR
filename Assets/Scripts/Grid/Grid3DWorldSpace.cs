using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Grid3DWorldSpace<T> : Grid3D<T> where T : Component
{
	[SerializeField]
	private Vector3 _offset;
	public Vector3 Offset
	{
		get => _offset;
		set
		{
			_offset = value;
			SetInWorldSpace();
		}	
	}

	[SerializeField]
	private Vector3 _cellSize;
	public Vector3 CellSize
	{
		get => _cellSize;
		set
		{
			_cellSize = value;
			SetInWorldSpace();
		}
	}

    [SerializeField]
    private Quaternion _rotation;
    public Quaternion Rotation
    {
        get => _rotation;
        set
        {
            _rotation = value;
            SetInWorldSpace();
        }
    }

    public Grid3DWorldSpace() : this(10, 10, 10) { }

	public Grid3DWorldSpace(Vector3Int size) : this(size.x, size.y, size.z) { }

	public Grid3DWorldSpace(int width, int height, int length) : base(width, height, length)
	{
		_offset = Vector3.zero;
		_cellSize = Vector3.one;
		_rotation = Quaternion.identity;
    }

	public void SetOffsetCellSizeAndRotation(Vector3 offset, Vector3 cellSize) => SetOffsetCellSizeAndRotation(offset, cellSize, Quaternion.identity);

	public void SetOffsetCellSizeAndRotation(Vector3 offset, Vector3 cellSize, Quaternion rotation)
	{
		_offset = offset;
		_cellSize = cellSize; 
		_rotation = rotation;
		SetInWorldSpace();
	}

	public Vector3 ToWorldSpace(Vector3Int coords) => ToWorldSpace(coords.x, coords.y, coords.z);

	public Vector3 ToWorldSpace(float x, float y, float z) => _offset + _rotation * Vector3.Scale(new Vector3(x, y, z), _cellSize);

    private void SetInWorldSpace()
	{
		for (int i = 0; i < _array.GetLength(0); i++)
			for (int j = 0; j < _array.GetLength(1); j++)
				for (int k = 0; k < _array.GetLength(2); k++)
				{
					if(!IsNull(i, j, k))
						_array[i, j, k].transform.position = _offset + _rotation * Vector3.Scale(new Vector3(i, j, k), _cellSize);
                }
	}
}
