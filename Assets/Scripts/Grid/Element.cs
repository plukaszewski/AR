using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Element : MonoBehaviour
{
	public Type ElementType;

	public Mesh Mesh;
	public MeshRenderer MeshRenderer;
	public Color color;
	public Color highlightColor;

	public enum Type
	{
		O,
		X,
		C
	}

	public void Highlight()
	{
		MeshRenderer.material.color = highlightColor;
	}
}
