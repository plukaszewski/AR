using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator : MonoBehaviour
{
    [SerializeField]
    private MeshFilter _meshFilter;

    private int _x;
    private int _y;
    private int _z;
	private Element _prefab;
    private Element _spawned;

	private void Start()
	{
		Global.OnSubmit.AddListener(OnSubmit);
		Global.OnCancel.AddListener(OnCancel);
	}

	public void Add()
    {
        Global.OnCancel.Invoke();
        _spawned = Instantiate(_prefab, Global.Grid.ToWorldSpace(_x, _y, _z), Quaternion.identity);
        Global.OnNeedConfirmation.Invoke();
    }

    private void OnSubmit()
    {
		if (_spawned)
			Global.Game.Place(_x, _y, _z, _spawned);
	}

    private void OnCancel()
    {
        if(_spawned)
            Destroy(_spawned.gameObject);
    }

	public void Set(Vector3Int coords, Element item) => Set(coords.x, coords.y, coords.z, item);

	public void Set(int x, int y, int z, Element item)
    {
        _x = x;
        _y = y;
        _z = z;
        _prefab = item;
        _meshFilter.sharedMesh = item.Mesh;
	}
}
