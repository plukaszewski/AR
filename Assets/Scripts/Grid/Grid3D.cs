using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class Grid3D<T> : IEnumerable<T>
{
	[field: SerializeField]
	public int Width { get; private set; }

	[field: SerializeField]
	public int Height { get; private set; }

	[field: SerializeField]
	public int Length { get; private set; }


	protected T[,,] _array;

    protected readonly ReadOnlyCollection<Vector3Int> NEIGHBOURS = new ReadOnlyCollection<Vector3Int>(new Vector3Int[]
    {
        new (1, 0, 0), new (-1, 0, 0),
        new (0, 1, 0), new (0, -1, 0),
        new (0, 0, 1), new (0, 0, -1),
    });


    public ref T this[int x, int y, int z] => ref _array[x, y, z];
	public ref T this[Vector3Int coords] => ref _array[coords.x, coords.y, coords.z];


	public Grid3D() : this(10, 10, 10) { }

	public Grid3D(Vector3Int size) : this(size.x, size.y, size.z) { }

	public Grid3D(int width, int height, int length)
	{
		Width = width;
		Height = height;
		Length = length;

		_array = new T[Width, Height, Length];
    }


	//IEnumerable<T>
	public IEnumerator<T> GetEnumerator()
	{
	   return (IEnumerator<T>)_array.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return _array.GetEnumerator();
	}
	//~IEnumerable<T>

	public IEnumerable<T> GetNeightbours(int x, int y, int z)
	{
		int i = 0;
        Vector3Int coords = new (x, y, z);

        while (i < 6)
		{
            Vector3Int newCoords = coords + NEIGHBOURS[i++];
			if (IsValid(newCoords))
				yield return this[newCoords];
		}
	}



	public IEnumerable<Vector3Int> GetNeightboursCoords(Vector3Int coords) => GetNeightboursCoords(coords.x, coords.y, coords.z);

    public IEnumerable<Vector3Int> GetNeightboursCoords(int x, int y, int z)
	{
        int i = 0;
        Vector3Int coords = new(x, y, z);

         while (i < 6)
        {
            Vector3Int newCoords = coords + NEIGHBOURS[i++];
            if (IsValid(newCoords))
                yield return newCoords;
        }
    }


    public bool IsValid(Vector3Int coords) => IsValid(coords.x, coords.y, coords.z);

	public bool IsValid(int x, int y, int z)
	{
		return x >= 0 && x < Width
			&& y >= 0 && y < Height
			&& z >= 0 && z < Length;
	}

	public bool IsNull(int x, int y, int z) => _array[x, y, z] == null;
	public bool IsNull(Vector3Int coords) => this[coords] == null;

	public bool TryGet(int x, int y, int z, out T outItem)
	{
		if (!IsValid(x, y, z))
		{
			outItem = default;
            return false;
		}

		if (IsNull(x, y, z))
		{
            outItem = default;
            return false;
        }

		outItem = _array[x, y, z];
		return true;
	}

	public virtual void Remove(Predicate<T> predicate)
	{
        for (int i = 0; i < _array.GetLength(0); i++)
            for (int j = 0; j < _array.GetLength(1); j++)
                for (int k = 0; k < _array.GetLength(2); k++)
				{
					if (predicate(_array[i, j, k]))
					{
						_array[i, j, k] = default;
					}
				}
	}

	public IEnumerable<T> AllNotNull
	{
		get
		{
            for (int i = 0; i < _array.GetLength(0); i++)
                for (int j = 0; j < _array.GetLength(1); j++)
                    for (int k = 0; k < _array.GetLength(2); k++)
					{
						if(!IsNull(i, j, k))
							yield return _array[i, j, k];
					}

        }
	}

	public IEnumerable<Vector3Int> AllNotNullCoords
	{
		get
		{
			for (int i = 0; i < _array.GetLength(0); i++)
				for (int j = 0; j < _array.GetLength(1); j++)
					for (int k = 0; k < _array.GetLength(2); k++)
					{
						if (!IsNull(i, j, k))
							yield return new Vector3Int(i, j, k);
					}

		}
	}
}
