using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GridEditor : MonoBehaviour
{
	public Element Origin;
	public UnityEvent OnChange { get; } = new();
	public Transform DefaultParent;

	private void Awake()
	{
        //Global.GridEditor = this;
	}

	private void Start()
	{
		Global.Grid[Vector3Int.zero] = Instantiate(Origin);
		OnChange.Invoke();
	}

	public void Add(Element prefab, int x, int y, int z)
	{
        Global.Grid[x, y, z] = Instantiate(prefab, Global.Grid.ToWorldSpace(x, y, z), Quaternion.identity, DefaultParent);
		OnChange.Invoke();
	}
}
